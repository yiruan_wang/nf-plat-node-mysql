// 实现添加服务

// 引入help
const { getModel } = require('../../packages/mysql.js')

console.log('\n getOne 文件被加载\n')

/**
 * 实现获取记录服务
 * @param {object} userInfo 当前登录人的信息
 * @param {object} help 访问数据库的实例
 * @param {objec} serviceInfo 服务的 meta
 * @param {object} model 占位用
 * @param {number|string} id 记录ID
 * @returns 返回新添加的记录的ID
 */
const getData = (userInfo, help, serviceInfo, model, id) => {
  return new Promise((resolve, reject) => {
    // 加载meta
    modelName = serviceInfo.model
    const info = require(`../../public/model/${modelName}.json`)

    console.log('\n启动 getOne 服务\n', info)
    getModel(help, info, id).then((model) => {
      // 添加成功
      console.log('获取数据:', model)
      resolve({ model })
    }).catch((err) => {
      reject('获取一条数据出错！')
    })

  })
}

module.exports = getData
