// 实现添加服务

// 引入help
const { getCount, getPager } = require('../../packages/mysql.js')

console.log('\n getPager 文件被加载\n')

/**
 * 实现获取记录服务
 * @param {object} userInfo 当前登录人的信息
 * @param {object} help 访问数据库的实例
 * @param {objec} serviceInfo 服务的 meta
 * @param {object} pagerInfo 分页和查询信息
 * @returns 返回新添加的记录的ID
 */
const getPager1 = (userInfo, help, serviceInfo, pagerInfo) => {
  return new Promise((resolve, reject) => {
    // 加载meta
    modelName = serviceInfo.model
    const info = require(`../../public/model/${modelName}.json`)

    console.log('\n启动 getPager 服务\n') // , info
    const query = pagerInfo.query
    const pager = info.pager
    pager.pagerIndex = pagerInfo.pager.pagerIndex
    console.log('分页信息:', pager)

    // 返回对象
    const re = {
      list: [],
      pager: info.pager
    }

    if (pagerInfo.useCount) {
      // 需要统计总记录数
      getCount(help, info, query).then((count) => {
        console.log('外部获取总记录数:', count)
        // 设置总记录数
        re.pager.pagerTotal = count
        // 获取分页记录集
        getPager(help, info, query, pager).then((list) => {
          console.log('获取分页数据1:', list)
          re.list = list
          resolve(re)
        }).catch((err) => {
          reject('获取分页数据出错！')
        })
      }).catch((err) => {
        reject('获取总记录数出错！')
      })
    } else {
      // 直接获取记录集
      getPager(help, info, query, pager).then((list) => {
        console.log('获取分页数据2:', list)
        re.list = list
        resolve(re)
      }).catch((err) => {
        reject('获取分页数据出错！')
      })
    }
  })
}

module.exports = getPager1
