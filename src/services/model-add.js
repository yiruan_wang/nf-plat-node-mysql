// 引入help
const { addModel } = require('../../packages/mysql.js')

console.log('\n★ add 文件被加载\n')

/**
 * 实现添加服务
 * @param {object} userInfo 当前登录人的信息
 * @param {object} help 访问数据库的实例
 * @param {objec} serviceInfo 服务的 meta
 * @param {object} model 前端提交的 body
 * @returns 返回新添加的记录的ID
 */
const add = (userInfo, help, serviceInfo, model) => {
  return new Promise((resolve, reject) => {
    // 加载meta
    modelName = serviceInfo.model
    const info = require(`../../public/model/${modelName}.json`)

    console.log('\n启动 add 服务\n')
    addModel(help, info, model).then((newId) => {
      console.log('外部添加数据:', newId)
      resolve({ newId })
    }).catch((err) => {
      reject('添加数据出错！')
    })

  })
}

module.exports = add
