/**
 * 实现服务
 * 传入数据包
 * 分析服务类型
 * 调用具体的实现文件
 */

// 引入help
const { MySQLHelp } = require('../../packages/mysql.js')
// 获取配置信息
const config = require('../../public/config/mysql-test.json')
// 加载服务目录
const serverList = require('../../public/service/list.json')
// 创建实例
const help = new MySQLHelp(config)

/**
 * 实现服务的第一层，判断服务类型
 * @param {object} userInfo 当前登录人的信息
 * @param {number|string} moduleId 模块ID
 * @param {number|string} actionId 动作ID
 * @param {number|string} dataId 数据记录的主键值
 * @param {object} body 提交的数据（post、body）
 * 
 */
function servers(userInfo, moduleId, actionId, dataId, body) {
  // 加载服务meta，分析服务类型
  const serverMeta = require(`../../public/service/${serverList[moduleId]}.json` )
  console.log('\n--serverMeta---', serverMeta.moduleName)

  // 小服务，判断类型，调用对应的代码
  return new Promise((resolve, reject) => {
    // 根据服务ID，加载meta
    const action = serverMeta.actions[actionId]
    const server = require(`./${action.kind}.js` )
    server(userInfo, help, action, body, dataId).then((res) => {
      resolve(res)
    }).catch((err) => {
      reject(err)
    })
  })
}

module.exports = servers
