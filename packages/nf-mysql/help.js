// 引入 MySQL
const mysql = require('mysql')

/**
 * 基于 promise 封装 MySQL 的基本操作，支持事务
 * * 创建数据库连接
 * * 事务相关
 * * 提交SQL给数据库执行，得到返回结果
 * * 共用函数
 * * info 结构：
 * * * host: 'localhost',
 * * * port: '3306',
 * * * user: 'root',
 * * * password: '',
 * * * database: '',
 * * * connectionLimit: 20
 */
class MySQLHelp {
  constructor (info) {
    // 创建普通连接的参数
    this._info = {
      host: 'localhost',
      port: '3306',
      user: 'root',
      password: '',
      database: ''
    }
    // 创建池子的参数
    this._infoTran = {
      host: 'localhost',
      port: '3306',
      user: 'root',
      password: '',
      database: '',
      connectionLimit: 20
    }
    
    // 加参数
    Object.assign(this._info, info)
    Object.assign(this._infoTran, info)
   
    // 预定一个池子，用于事务
    this.pool = null
    
    console.log(' ★ 初始化：不使用事务！')
    // 不使用事务，开启连接获得数据库对象，其实也支持事务
    this.db = mysql.createConnection(this._info)
    //启动连接
    this.db.connect((err) => {
      if(err) {
        console.error('连接数据发生错误：', err)
      } else {
        console.log('★ [MySQL 已经打开数据库，threadId：]', this.db.threadId)
      }
    })
  }

  /**
   * 在池子里面获取一个链接，创建对象
   */
  _poolCreateConnection() {
    const myPromise = new Promise((resolve, reject) => {
      console.log('初始化：使用事务')
      // 如果没有池子则创建一个。好在不是异步
      if (this.pool === null) {
        this.pool = mysql.createPool(this._infoTran)
      }
      // 从池子里面获取一个链接对象，这个是异步
      this.pool.getConnection((err, connection) => {
        // console.log('打开池子取新连接，threadId：', connection.threadId)
        if(err) {
          console.error(`pool 开启错误：${err.stack}；链接ID：${connection.threadId}`)
          reject(err)
        } else {
          resolve(connection)
        }
      })
    })
    return myPromise
  }


  /**
   * 内部开启事务
   */
  _beginStran(_cn) {
    const myPromise = new Promise((resolve, reject) => {
      _cn.beginTransaction((err) => {
        if (err) { //失败
          console.log('[★ ★ MySQL 开启事务时报错：] --- ', err)
          reject(err)
          return
        }
        console.log('[★ MySQL 事务已经开启：] - ')
        resolve(_cn)
      })
    })
    return myPromise
  }

  /**
   * 开启事务，await 的方式
   */
  async beginTransaction() {
    console.log('★ 开启事务，await 模式')
    let _cn = null
    try {
      _cn = await this._poolCreateConnection()
      await this._beginStran(_cn)
    } catch(e) {
      console.log('async 开启事务出错：', e)
    }
    return _cn
  }

  /**
   * 开启一个事务，Promise 的方式
   * @returns Promise 形式
   */
  begin() {
    const myPromise = new Promise((resolve, reject) => {
      // 如果还没有对象，从池子里面创建一个。
      console.log('★ 开启事务，promise 模式')
      this._poolCreateConnection().then((_cn) => {
        // 开启事务
        this._beginStran(_cn)
          .then(() => {resolve(_cn)}) // 返回连接对象
          .catch((err) => {reject(err)})
      })
    })
    return myPromise
  }

  /**
   * 提交一个事务
   * @param { connection } cn 开启事务时创建的连接对象
   * @returns 提交事务
   */
  commit(_cn) {
    const myPromise = new Promise((resolve, reject) => {
      // 提交事务
      _cn.commit((err) => {
        if(err) {
          console.log('事务提交失败', err)
          reject(err)
        } else {
          resolve()
        }
      })
    })
    return myPromise
  }

  /**
   * 把 SQL 提交给数据库。支持事务。
   * @param { string } sql sql语句
   * @param { array } params 需要的参数，数组形式
   * @param { connection } cn 如果使用事务的话，需要传递一个链接对象进来
   * @returns Promise 形式
   */
  query(sql, params=[], cn = this.db) {
    const myPromise = new Promise((resolve, reject) => {
      // 把SQL语句提交给数据库执行，然后等待回调
      try {
        cn.query(sql, params, (err, res, fields) => {
          if (err) { //失败
            console.log('[MySQL sql] --- ', sql)
            // console.log('[MySQL ERROR] - ', err.message)
            // 如果开始事务，自动回滚
            if (cn !== this.db) {
              console.log('[MySQL 执行SQL失败，数据回滚] ---------------- ')
              cn.rollback((err) => {
                console.log('执行SQL失败，数据回滚：', err)
              })
            }
            reject(err)
            return
          }
          // console.log('[MySQL 返回：] - ', res)
          // console.log('[MySQL fields：] - ', fields)
          resolve(res)
        })
      } catch (err) {
        console.log('出现异常：',err)
      }
      
    })
    return myPromise
  }

  
  /**
   * 关闭数据库
   * @param { connection } cn 开启事务时创建的连接对象
   */
  close(_cn = null) {
    if (_cn !== null ) {
      // 归还连接对象。console.log('--close: _cn', _cn.threadId)
      _cn.release()
      console.log('\n[MySQL 事务，已经关闭数据库：] \n')
    } else {
      // 关闭连接
      this.db.end((err) => {
        if(err) {
          console.error('关闭连接发生错误：', err)
        } else {
          console.log('\n[MySQL 已经关闭数据库：]\n')
        }
      })
    }
  }
  
  /**
   * 关闭池子
   */
  closePool() {
    this.pool.end((err) => {
      if(err) {
        console.error('关闭=连接发生错误：', err)
      } else {
        console.log('\n[MySQL 已经关闭连接池：]\n')
      }
    })
  }

  // 下面是内部函数 =====================

  /**
   * 依据信息处理字段名和值
   * @param {object} info 字段名
   * @param {object} values 值
   * @returns 字段名称和值的数组
   * * info 结构：
   * * * tableName： '', 表名
   * * * cols：{colName: '类型'}, josn 字段需要标记 
   * * values 结构：
   * * * colName: value 
  */
  createTableCols(info, values) {
    /**
     * 记录字段名称。
     * * 数组
     */
    const colNames = []
    // 记录对应的值
    const params = []

    // 变量对象，记录 key和 value
    for (const key in info.cols) {
      colNames.push(key + '=? ')
      // 判断类型
      switch(info.cols[key]){
        case 'json':
          // json 格式的字段，需要转换成字符串才能保存
          switch (typeof values[key]){
            case 'object':
              params.push(JSON.stringify(values[key]))
              break
            case 'string':
              params.push(values[key])
              break
          }
          break
        default:
          params.push(values[key])
          break
      }
    }

    return {
      colNames,
      params
    }
  }

  /**
   * 内部函数，根据查询条件，拼接查询用的SQL语句，where 后面的部分。
   * @param {object} query 查询条件
   * @returns where 后面的查询语句
   */
  _getWhereQuery(query) {
    // 查询条件
    const findKind = {
      // 字符串
      401: ' {col} = ? ',
      402: ' {col} <> ? ',
      403: ' {col} like ? ',
      404: ' {col} not like ? ',  
      405: ' {col} like ? ', // 起始于
      406: ' {col} like ? ', // 结束于
      // 数字
      411: ' {col} = ? ',
      412: ' {col} <> ? ',
      413: ' {col} > ? ',
      414: ' {col} >= ? ',
      415: ' {col} < ? ',
      416: ' {col} <= ? ',
      417: ' {col} between ? and ? ',
      // 日期
      421: ' {col} = ? ',
      422: ' {col} <> ? ',
      423: ' {col} > ? ',
      424: ' {col} >= ? ',
      425: ' {col} < ? ',
      426: ' {col} <= ? ',
      427: ' {col} between ? and ? ',
      // 范围
      441: ' {col} in (?)'
    }
    const _whereCol = [] // 查询字段
    const _whereValue = [] // 查询参数
    // 设置查询条件
    for (const key in query) {
      const val = query[key]
      _whereCol.push(findKind[val[0]].replace('{col}', key))
      switch (val[0]) {
        case 403: // like
        case 404: // not like
          _whereValue.push('%' + val[1] + '%')
          break
        case 405: // like a%
          _whereValue.push(val[1] + '%')
          break
        case 406: // like %a
          _whereValue.push('%' + val[1])
          break
        case 417: // between 数字
        case 427: // between 日期
          _whereValue.push(...val[1])
          break
        case 441: // in
          _whereCol[_whereCol.length - 1] =
            _whereCol[_whereCol.length - 1]
              .replace('?', val[1].map(a => '?').join(','))
          _whereValue.push(...val[1])
          break
        default:
          _whereValue.push(val[1])
          break
      }
    }
    // 如果没有查询添加，设置 1=1 占位
    if (_whereCol.length === 0) {
      return {
        whereQuery: '',
        whereValue: []
      }
    } else {
      return {
        whereQuery: ' WHERE ' + _whereCol.join(' and '),
        whereValue: _whereValue
      }
    }
  }

  
  /**
   * 内部函数，根据分页信息，设置 分页信息和排序字段
   * @param { object } pager 分页和排序
   * @returns order by 和 limit
   */
  _getPager (pager) {
    let _limit = '' // 分页，可以不设置
    let _order = '' // 排序，可以不设置
    // 设置分页 order by 和 limit
    if (typeof pager !== 'undefined') {
      if (typeof pager.pagerIndex !== 'undefined') {
        // 用 limit 实现分页
        const _pageSize = pager.pagerSize || 10
        const index = _pageSize * (pager.pagerIndex - 1)
        _limit = ` limit ${index}, ${_pageSize} `
      }
      if (typeof pager.orderBy === 'object') {
        // 设置排序字段和方式
        const arr = []
        for(const key in pager.orderBy ) {
          const col = key
          const isAsc = pager.orderBy[key] ? ' asc ' : ' desc '
          arr.push(col + ' ' + isAsc)
        }
        _order = ' order by ' + arr.join(',')
      }
    }

    return {
      orderBy: _order,
      limit: _limit
    }
  }

}

module.exports = MySQLHelp