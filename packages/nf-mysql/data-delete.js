
/**
 * 实现删除数据的功能。拼接 DELETE FROM 的 SQL语句
 * @param { MySQLHelp } help 访问数据库的实例
 * @param { Object } info 表、字段
 * @param { number|string } id 数据
 * @returns 影响的记录数
 * * info 结构：
 * * * tableName： '', 表名
 * * * id '', 主键名称
 * * * cols：{colName: '类型'}, josn 字段需要标记 
 * * id 结构：number string
 */
function deleteData(help, info, id, cn = null) {
  // 拼接 修改 用的SQL语句，
  // 提交SQL语句
  console.log('')
  console.log('deleteData，开始运行 :')
  const myPromise = new Promise((resolve, reject) => {
    const sql = `DELETE FROM ${info.tableName} WHERE ${info.idKey} = ? `
    console.log('DELETE --- sql：', sql, [id])
    const _cn = cn === null ? help.db : cn

    help.query(sql, [id], _cn)
      .then((res) => {
        // 删除成功，返回影响行数
        // console.log('删除数据成功:', res)
        resolve(res.affectedRows)
      })
      .catch((res) => {
        // 出错了
        reject(res)
      })
    })
  return myPromise
  
}

module.exports = deleteData