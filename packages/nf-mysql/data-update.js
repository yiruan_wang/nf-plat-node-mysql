
/**
 * 实现添加数据的功能。拼接 UPDATE 的 SQL语句
 * @param { MySQLHelp } help 访问数据库的实例
 * @param { Object } info 表、字段
 * @param { Object } values 数据
 * @param { number|string } id 数据
 * @returns 影响的记录数
 * * info 结构：
 * * * tableName： '', 表名
 * * * id '', 主键名称
 * * * cols：{colName: '类型'}, josn 字段需要标记 
 * * values 结构：
 * * * colName: value 
 * * id 结构：number string
 */
function updateData(help, info, values, id, cn = null) {
  // 拼接 修改 用的SQL语句，
  // 提交SQL语句
  console.log('updateData，开始运行 :')
  const myPromise = new Promise((resolve, reject) => {
    // sql = 'UPDATE aaa set aaacol = ? WHERE id=?'
    // 获取字段名称和值的数组
    const { colNames, params } = help.createTableCols(info, values)
    const sql = `UPDATE ${info.tableName} SET ${colNames.join(',')} WHERE ${info.idKey} = ? `
    params.push(id)

    console.log('UPDATE --- sql：', sql, params)
    const _cn = cn === null ? help.db : cn

    help.query(sql, params,_cn)
      .then((res) => {
        // 修改成功，返回影响行数
        // console.log('修改数据成功:', res)
        resolve(res.affectedRows)
      })
      .catch((res) => {
        // 出错了
        reject(res)
      })
    })
  return myPromise
  
}

module.exports = updateData