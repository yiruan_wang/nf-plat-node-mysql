
/**
 * 实现添加数据的功能。拼接 insert 的 SQL语句
 * @param { MySQLHelp } help 访问数据库的实例
 * @param { Object } meta 表、字段
 * @param { Object } model 数据
 * @param { connection } cn 如果使用事务的话，需要传递开启事务时创建的连接对象
 * @returns 添加记录的ID
 * * meta 结构：
 * * * tableName： '', 表名
 * * * cols：{colName: '类型'}, josn 字段需要标记 
 * * model 结构：
 * * * colName: value 
 */
function addData(help, meta, model, cn = null) {
  // 拼接添加用的SQL语句，
  // 提交SQL语句
  // console.log('addData，开始运行 :')
  const myPromise = new Promise((resolve, reject) => {
    // sql = 'INSERT INTO aaa set aaacol = ? '
    // 获取字段名称和值的数组
    const { colNames, params } = help.createTableCols(meta, model)
    const sql = `INSERT INTO ${meta.tableName} SET ${colNames.join(',')} `
    console.log('addData --- sql：', sql, params)
    const _cn = cn === null ? help.db : cn
    help.query(sql, params, _cn)
      .then((res) => {
        // console.log('添加数据成功:', res.insertId)
        resolve(res.insertId)
      })
      .catch((res) => {
        // 出错了
        reject(res)
      })
    })
  return myPromise
  
}

module.exports = addData