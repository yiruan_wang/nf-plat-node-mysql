
/**
 * 通过ID，获取一条记录
 * @param { MySQLHelp } help 访问数据库的实例
 * @param { Object } info 表、字段
 * @param { number|string } id 主键值
 * @returns 添加记录的ID
 * * info 结构：
 * * * tableName： '', 表名
 * * * IdName： '', 主键字段名
 * * * cols：{colName: ''}, 可以写 *  
 * * id ：主键值
 */
function getModel(help, info, id) {
  // 拼接添加用的SQL语句，
  // 提交SQL语句
  console.log('getModel，开始运行 :')
  const myPromise = new Promise((resolve, reject) => {
    // 获取字段名称和值的数组
    const sql = `SELECT ${Object.keys(info.cols).join(',')} FROM ${info.tableName} WHERE ${info.idKey} =? `
    console.log('getModel --- sql：', sql)

    help.query(sql, [id])
      .then((res) => {
        // 添加成功
        // console.log('获取一条数据:', res)
        resolve(res)
      })
      .catch((err) => {
        // 出错了
        console.log('获取一条数据失败了:', err)
        reject(err)
      })
    })
  return myPromise
  
}

module.exports = getModel