
/**
 * 获取全部记录
 * @param { MySQLHelp } help 访问数据库的实例
 * @param { Object } info 表、字段
 * @param { Object } query 查询条件
 * @returns 添加记录的ID
 * * info 结构：
 * * * tableName： '', 表名
 * * * cols：{colName: '类型'}, josn 字段需要标记 
 * * query 结构：
 * * * colName: value 
 */
function getAllData(help, info, query) {
  // 拼接添加用的SQL语句，
  // 提交SQL语句
  console.log('getAllData，开始运行 :')
  const myPromise = new Promise((resolve, reject) => {
    // 把查询条件变成SQL语句
    const { whereQuery, whereValue } = help._getWhereQuery(query)
    // 设置显示的字段
    const showCol = Object.keys(info.cols)
    if (showCol.length === 0 ) {showCol.push('*')}

    const sql = `SELECT ${showCol.join(',')} FROM ${info.tableName} ${whereQuery} `
    console.log('getAllData --- sql：', sql, whereValue)

    help.query(sql, whereValue)
      .then((res) => {
        // 添加成功
        // console.log('获取全部数据成功:', res)
        resolve(res)
      })
      .catch((err) => {
        // 出错了
        console.log('获取全部数据失败了:', err)
        reject(err)
      })
    })
  return myPromise
  
}

module.exports = getAllData