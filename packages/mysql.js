
// 合并各种操作，统一输出

const MySQLHelp = require('./nf-mysql/help.js')
const addModel = require('./nf-mysql/data-add.js')
const updateModel = require('./nf-mysql/data-update.js')
const deleteModel = require('./nf-mysql/data-delete.js')
const getModel = require('./nf-mysql/data-getone.js')
const getList = require('./nf-mysql/data-getall.js')
const { getCount, getPager } = require('./nf-mysql/data-getpager.js')

module.exports = {
  MySQLHelp,
  addModel,
  updateModel,
  deleteModel,
  getModel,
  getList,
  getCount,
  getPager
}